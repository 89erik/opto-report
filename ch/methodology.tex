\chapter{Methodology}\label{ch:methodology}
This chapter describes the technology that was developed and its utilization as experimental tools. The discussion centers around a material interface that we developed to electrically isolate the material from the surrounding Mecobo hardware. We begin in section \ref{ch:methodology:requirements} by introducing the purpose and requirements for such an interface. Following, in section \ref{ch:methodology:mecobo} we will take a closer look on this surrounding hardware, i.e. the Mecobo platform, and see how it works. 

A high-level introduction to the developed material interface is then presented in section \ref{ch:methodology:circuit} where we discuss its circuit design and how it works. Further, the process and result of implementing this circuit in hardware is documented in section \ref{ch:methodology:hardware}, and the steps taken to integrate it into Mecobo in section \ref{ch:methdology:interfacing_mecobo}.

Having the design and implementation of the interface covered, we will then, in section \ref{ch:methodology:problems}, document some of the problems arisen from the use of opto-isolators and how they affected the design process. Finally, in section \ref{ch:methodology:determinism}, the utilization and determinism of the interface used as an experimental tool is discussed.


\section{Requirements of interface}\label{ch:methodology:requirements}
The purpose of the material interface is ultimately to deny any \ac{GA} access to exploit any physical properties in the surrounding electronics of the material. The surrounding electronics, in this particular case, is the Mecobo platform. It is a relatively complex piece of hardware, containing both an \ac{FPGA} and a microcontroller. To avoid electrical side-effects of Mecobo to affect the material, the material must not be electrically connected to Mecobo. This could be achieved by designing and implementing a material interface that electrically isolates the material from Mecobo while allowing \ac{EiM} to function as normal. The chief component of the implementation of this interface would be the opto-isolator. It is an off-the-shelf component that transfers an electrical signal from one circuit to another by the use of light.

As this is the first effort to electrically isolate the material in \ac{EiM}, the task is simplified to only involve digital signals.

\subsection{Requirement 1} 
The first and foremost requirement of the material interface is directly related to the purpose of this thesis: \textbf{The interface should electrically isolate Mecobo from the computational material}. As mentioned in chapter \ref{ch:intro}, when the material and its ``surrounding hardware'', e.g. Mecobo, are not electrically isolated, the \ac{GA} cannot distinguish whether the behaviour it senses is a result of the material alone or the material in combination with the surrounding hardware. As stated by Lykkebø and Tufte, a challenge to EiM is ``the definition of the border between the apparatus applying configuration signals, and the material itself''\cite{mecobo_representations}. By electrically isolating the material from Mecobo, we can essentially draw such a defined border.

\subsection{Requirement 2}
\textbf{The interface should not significantly disrupt the possibility to perform \ac{EiM} on the material}. For instance, we should seek to maximize the interface's ability to transfer data and minimize its effect on the computational properties of the material. The interface must also remain fully controllable by the Mecobo system and not require manual intervention. The reason for this is to avoid placing restrictions on the \ac{GA}: A crucial aspect of \ac{EiM} is to explore intrinsic unknown properties in a component, and to transfer control of said component from the \ac{GA} to the designer would be contradictory. GA control of pin selection, i.e. which pins are input and output, is particularly relevant. The favoured approach in the design of Mecobo was to put pin selection under evolutionary control\cite[p.~271]{mecobo14}. 


\section{The Mecobo platform}\label{ch:methodology:mecobo}
Figure \ref{fig:mecobo} shows a block diagram and a photograph of the Mecobo platform.\footnote{The photograph by NASCENCE.} The \emph{Host} component of figure \ref{fig:mecobo:overview} represents a PC which is connected to the Mecobo board by USB. It can command Mecobo to apply signals to, or record output from, the material. Typically this PC either runs some \ac{GA} or it functions as a server exposing Mecobo to other PCs over the internet. The actual board, shown in figure \ref{fig:mecobo:photo} and enclosed in the blue square labeled \emph{EMB board} in figure \ref{fig:mecobo:overview}, has a \acf{FPGA} and a microcontroller as its main components. The microcontroller functions as a USB interface and receives the commands from the host PC. The microcontroller communicates these commands to the \ac{FPGA}, which in turn applies them to the material. The \ac{FPGA} functions as an interface to the material and is able to route any signal to or from any pin; there are no restrictions to whether a material pin is input or output.

\input{fig/background/mecobo}

Also included in figure \ref{fig:mecobo:overview} is the optional \emph{Daughter board} component. A daughter board can be placed in between the \ac{FPGA} and the material to add further functionality to the platform without having to redesign everything. In this thesis, the aforementioned material interface is implemented as such a daughterboard. A photograph of Mecobo with the final material interface implementation, named optowall, is shown in figure \ref{fig:methodology:all_hw}. In this photograph, the optowall daughterboard is plugged directly into Mecobo's 60 pin header north. The material slide, representing the \emph{Material Bay} component of figure \ref{fig:mecobo:overview}, is seen at the far right, as a part of optowall. 

\input{fig/methodology/all_hw}

\section{Circuit design of material interface}\label{ch:methodology:circuit}
The circuit of a single material pin interface is illustrated in figure \ref{fig:circuit}. This circuit is repeated for all connected pins to the material. A key aspect of requirement 2, i.e. not disrupting the possibility to perform EiM, is that every pin should be considered a duplex pin, i.e. one that can be used as both input and output. For this reason, all pins contains components both for applying input and reading output. 

Because the purpose of the interface is to separate the material from the Mecobo circuit, it consists of two separated circuits. The left hand side is the part which is connected to the Mecobo circuit. The right side, separated by the dashed line, is the material circuit. Data transfer between the Mecobo- and material-side circuits is made possible by three opto-isolators per material pin. The following sections describe the workings of the circuit by means of two use-cases: Reading output from the material, and applying input into the material.

\input{fig/methodology/circuit/full}

\subsection{Reading output from material}
At the bottom left of figure \ref{fig:circuit} the \emph{opto-out} component, comprised of one opto-isolator, connects material output to mecobo. A logically high voltage from the material lights the diode of the opto-isolator which is detected by the phototransistor on the Mecobo side. When the phototransistor is triggered by the light, it allows current to flow from VD+ to VD-. This causes the voltage at the \emph{out} signal to fall low. Inversely, \emph{out} will be high when there is no light from the diode, because the transistor will block the current and all the current from VD+ will flow to the \emph{out} signal instead. The \emph{out} signal is routed back to mecobo, and can be interpreted as an inverse ouput from the material. 

For reasons discussed shortly, the material output signals needs amplification to aid their translation through opto-out. This amplification is performed by the \emph{amplifier} component in the upper right corner of figure \ref{fig:circuit}. It is comprised of an operational amplifier with a feedback loop and a reference voltage controlled by two resistors. To control the reference voltage, a static voltage divisor was chosen over a potentiometer for two reasons. First, it is much easier to keep multiple amplifiers at the same setting without potentiometers, and second, the manual control gained of the signals by potentiometers would not be accessible to the \ac{GA} or any other software. The material pin, which in the output scenario represents the signal source, is connected into the amplifier which, on the other side, drives the opto-out component.

\subsection{Applying input to material}
The two top opto-isolators, labeled \emph{opto-in}, connects a signal to the material from Mecobo as input. There are two opto-isolators to allow a defined logical low. This is important because the material is essentially analogue; small differences in voltage can possibly change the behavior. This is as opposed to the digital Mecobo side, where as long as the voltage is measured to less than 0.8V, it is interpreted as logical low by the LVCMOS33 standard used on Mecobo\cite[p.~272]{mecobo14}. Because every material pin needs to be represented by two values at the input side of the opto-isolator, Mecobo needs to represent each input signal to the material as a two bit array where the rightmost bit represents the value of $in_0$ and the leftmost $in_1$. The relationship between input arrays from Mecobo and the values that are applied to the material pin is then as in table \ref{tbl:opto-in}, where the least significant bit represents in0.

\begin{table}
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Mecobo input array} & \textbf{Material pin value} \\
        \hline
        01 & 0 \\
        10 & 1 \\
        00 & Z \\
        \hline
    \end{tabular}\\
    \caption{Relationship between input and output of the opto-in opto-isolator}
    \label{tbl:opto-in}
\end{table}

Let us take the input string of 01 as an example. This input causes the upper phototransistor to close, meaning that current can not flow from VA+. Additionally, the lower phototransistor opens, meaning that any potential on the material pin is drawn to ground (VA-), effectively turning the pin into a sink. Inversely, an input of 10 allows current from VA+ to enter but not to exit through VA- and is thus forced to enter the material. Additionally a state of high impedance (Z) can be achieved by inputing 00 which closes both phototransistors, essentially disconnecting the material pin from the input opto-isolators. This latter state is particularly useful when the pin should be used as an output pin. 

\section{Hardware realization of material interface}\label{ch:methodology:hardware}
The material interface is realized by two boards, the optowall daughterboard and the optoamp extension board. Plugged together, they consist of 16 instances of the circuit introduced above, making them capable of connecting up to 16 material pins. 

The optowall daughterboard is the main tool. It is designed as a daughterboard for Mecobo and can be plugged directly into one of its I/O connectors. It is also the board in which the material is connected. The optoamp extension board is a later addition made up chiefly of operational amplifiers that can amplify output signals. In addition to the designing of these boards, we have also hand soldered and validated both boards and, to some degree, Mecobo. Software was also developed to allow a quick, easy, and automated utilization of the hardware tools. As mostly irrelevant to the discussion, they are only discussed in appendix \ref{apx:software}. The schematics of both PCBs are included in appendix \ref{apx:schematics}.

\subsection{Optowall}
From the perspective of evolution, the optowall daugther board is essentially a wall denying access to anything else than the material for exploitation. It consists of a \ac{PCB} with 12 quad channel opto-isolators, a connector for an electrode glass slide containing the computational material, connectors to external power supplies, various jumper connectors for increased flexibility, and a large bus connector that is compatible with two of Mecobo's three I/O headers. 

\input{fig/methodology/optowall}

A photograph of the board is shown in figure \ref{fig:optowall}. It is organized as follows: The main I/O bus connector is placed at the far left side. Most of the pins are used for data lines, except those that are either power pins or not used at either Mecobo headers Header North or Header West. All power pins are routed to two optional power connectors which are discussed below. The central part of the board consists of four groups of opto-isolator circuits. Each group has two quad channel opto-isolators for \emph{opto-in} and one for \emph{opto-out}. Each group connects to four material pins. 

\subsubsection{Power considerations}
Because optowall is essentially two separated circuits, both circuits needs their own power supply and the \ac{PCB} is designed to support this in a flexible way. There are four power nets in total which all have their own power plane inside the PCB: Digital high and ground for the Mecobo-side circuit, and analogue high and ground for the circuit side. Power can be supplied to the nets by connecting external power supply cables to the power supply header on the bottom right corner of the PCB, which is shown in figure \ref{fig:power}. This header has two pins for each power net so that power nets also can be connected together through jumpers. For instance could both ground nets be connected so that both circuits would share the same ground. 

\input{fig/methodology/power}
\input{fig/methodology/mecobopower}

Since the Mecobo-side circuit transfers data to and from Mecobo, the digital power should in most cases come directly from Mecobo and not an external power supply. The two headers HWest\_power\_connector and HNorth\_power\_connector in the lower left corner of the PCB can draw power from Mecobo header west and north respectively. These headers, shown in figure \ref{fig:methodology:mecobopower}, allows Mecobo's power and ground to be connected to either of optowall's power and/or ground planes, AGND, VA+, DGND, and VCC, by connecting jumpers on the corresponding pins.

\subsection{Configuration flexibility}
On the material side, both the input and output opto-couplers are connected to the material through a header connector labeled ``material connector''. This was added as a safety feature in case there would be any problems attaching a material pin to both input and output at the same time. If so, we could simply remove the jumper that connects them. In figure \ref{fig:circuit}, these jumpers are the two square boxes connecting to the material. Additionally, as was later proven useful, extra components such as amplifiers could be connected to the material interface at these connectors. The ground signal on the diode on the opto-out opto-isolator can also optionally be connected by jumpers to VA+ instead of VA-. This would allow the diode to be considered a part of the computational medium, which might be an interesting configuration for the in materio evolution.

\subsection{Optoamp}
To facilitate amplification of all material pins in a stable and reliable manner, we designed another board, the optoamp extension board. It mimics the design of optowall by using four quad channel operational amplifiers to amplify all 16 material pins. It can be stacked onto optowall through its material connectors, as shown in figure \ref{fig:methodology:wall-amp}. It gets its data through half of the pins on the material connector representing the connection between material and opto-in. These headers are illustrated in figure \ref{fig:circuit} as the solid-line square box connecting to the material. To also allow input signals from opto-in to enter the material, these pins should be connected by jumpers as usual, and optoamp facilitates this by extending the header so that the jumpers can instead be placed on optoamp with the same effect. The remaining pins, which are represented by the stippled lined box of figure \ref{fig:circuit}, should not be connected, so that opto-out is only connected to the output of the amplifier, and not the material directly. As the board would need to be placed physically on top of optowall, it would also block two of the opto-out GND connectors located in between the material connectors on optowall, making it difficult to place jumpers on them. Therefore, these connectors were also extended to optoamp. Conveniently, these connectors contain both analog voltage supply and analog ground, and are thus used as power supply for optoamp. 

\input{fig/methodology/wall-amp}

\section{Integrating the optowall daughterboard with Mecobo}\label{ch:methdology:interfacing_mecobo}
Because of optowall's unusual I/O scheme, as discussed in section \ref{ch:methodology:circuit}, some sort of interface was required to integrate optowall with Mecobo. This section describes how this integration was achieved.

\subsection{Integrating Mecobo and optowall through software}
Initially, it was attempted to interface Mecobo and optowall through software on the client side. Every input value to be sent to the material would first be mapped to a function equal to table \ref{tbl:opto-in}, returning its corresponding two bit value. Second, the material pin to be written to would be mapped to a function returning the corresponding two HNorth input pins on Mecobo that connects to the given material pin. Finally, a command would be sent to Mecobo that applied the mapped values to the mapped pins which would assert the proper value on the proper material pin. This scheme worked fine when applying constant voltages. With waves, however, the lack of accurate timing on Mecobo's scheduler was a critical problem. The source of the problem was located in the preexisting software on Mecobo's microcontroller. It would apply signals to the \ac{FPGA} by continually picking \emph{items}, each representing a signal to be applied to or read from a pin, from a queue in an infinite loop. 

\begin{lstlisting}[language=c, float=h, caption={Item execution on Mecobo's microcontroller}, label={lst:worker}]
for (;;) {
    ...
    struct pinItem * currentItem = &(itemsToApply[itaPos]);
    //Is it time to start the item at the head of the queue?
    if (currentItem->startTime <= timeMs) {
        execute(currentItem);
        ...
    }
    ...
}
\end{lstlisting}

As shown in listing \ref{lst:worker}, the loop body would compare the item's start time with the current time and, if the start time was less than or equal, execute the item. Not shown in the listing is a lot of additional work that was performed at every iteration. As the timing of this time comparison depended on the variable amount of work performed in every loop iteration, items would be executed at unpredictable times after their scheduled start time.\footnote{NASCENCE has subsequently fixed this problem.}

\input{fig/methodology/outofsync_wave/outofsync_wave}

Because of the nature of the optowall circuit (discussed in \ref{ch:methodology:circuit}), applying a wave to the material through optowall would require two opposing square waves so that while one was high the other should always be low. As illustrated by figure \ref{fig:outofsync_wave}, to properly represent the output wave, the input waves could never be \emph{both} low or high. Figure \ref{fig:outofsync_wave:a} shows that two perfectly opposing input waves on each of the input opto-isolators, denoted in1 and in0, produces a clean output wave onto the material pin. In the case of a skew on an input wave, as illustrated in figure \ref{fig:outofsync_wave:b}, the output wave will contain several illegal states, marked by the grey boxes. The first of such states, having both input waves momentarily low, will cause the output to go to a state of high impedance, which differs from the desired defined low. When both inputs are high, the output goes into a state that was never intended to be used. This state would cause both phototransistors to open, allowing current to flow almost freely from the voltage source to ground, possibly damaging the phototransistors. As the microcontroller would execute the two opposing waves at unpredictable times, it was impossible to reliably achieve exactly opposing waves in practice. To fulfill this requirement, the FPGA would need to be modified. 

\subsection{Modifications to the Mecobo FPGA}
The modifications to the FPGA would move the responsibility of interfacing optowall's unusual I/O scheme from the client software past the microcontroller to the FPGA. In addition to circumnavigating the microcontroller's timing issues, having the FPGA perform all pin abstractions would make optowall compatible with all preexisting software. 

The changes to the FPGA mainly concerned its \var{pincontrol} module. Originally, each instance of this module would contain a reference to one physical pin and signals to control whether the pin should function as input or output.\footnote{In this subsection, ``input'' and ``output'' is relative to Mecobo.} When output was enabled, the value on the pin would be driven by a state machine, \var{pin\_output}, which would drive the signals instructed by the microcontroller. The module was changed so that instead of one duplex pin per pin controller, it would have one input and two output pins. The outputs would then be driven according to table \ref{tbl:opto-in} (page \pageref{tbl:opto-in}): When output was enabled it would drive one pin to the value of the state machine, and the other to its inverse. Otherwise, both pins would be driven to zero, causing a state of high impedance on the material pin. Additionally, it would invert the input, as this signal is inverted by the opto-out component. Listing \ref{lst:pincontroller} shows the key modifications in Verilog code. The main clause of the compiler directive \var{ifdef} shows the added code while the else clause shows the preexisting code. Additional code elsewhere maps the proper pins to the modified pin controllers. 

\begin{lstlisting}[language=verilog, float=h, caption={FPGA pin controller modifications}, label={lst:pincontroller}]
`ifdef WITH_OPTOWALL
    //Drive output pins to 01 (high), 10 (low) or 00 (Z)
    assign pins_out  = (enable_pin_output)? 
            ((pin_output)?  2'b01 : 2'b10): 2'b00;
    assign pin_input = ~pin_in;
`else
    //Drive output pin from pin_output state machine if output
    assign pin = (enable_pin_output) ? pin_output : 1'bZ; //Z or 0
    //else we have input from pin.
    assign pin_input = pin;
`endif
\end{lstlisting}

With this solution, there was no observable timing issues when viewed at an oscilloscope with 5 ns resolution. As the responsibility of correctly timing the two waves was moved to the FPGA, the timing issues of the microcontroller could be disregarded. 


\section{Problems arising from the use of opto-isolators}\label{ch:methodology:problems}
Because the nature of the material as a computational medium is inherently sensitive, applying opto-isolation to its interface introduced some issues. This section addresses these issues and describes how they where counteracted.

\subsection{Voltage drop on material output}
Initially, the material interface was implemented without any amplification of the output signals. During the initial testing of the interface, which at the time existed solely in the form of the optowall daughterboard, it was discovered that as more material pins were connected to the opto-isolators, the amplitude of output signals became lower. 

\input{fig/methodology/opto_disconnection_Z/all}

To investigate this phenomena, we sent a square wave into pin 0 and measured the output on pin 1 with four different jumper configurations on pin 2, i.e. the closest unused pin. All remaining pins, pin 3 through 15, had all of their opto-isolators, i.e. both opto-in and opto-out, disconnected at all times. The output waves in the four different configurations are shown in figure \ref{fig:opto-disconnect}. 

The first configuration, whose output is shown in figure \ref{fig:opto-disconnect-a}, functions as a negative control, i.e. a configuration in which no phenomena is expected. In this configuration pin 2 has neither of its opto-isolators connected, and thus the active pins, i.e. pin 0 and 1, are the only ones that are connected to opto-isolators.

In the three following configurations, pin 2 is connected to only opto-in (figure \ref{fig:opto-disconnect-b}), only opto-out (figure \ref{fig:opto-disconnect-c}), and finally both opto-out and opto-in (figure \ref{fig:opto-disconnect-d}). From these graphs it is clear that having more opto-isolators on the circuit, even though they are not actively used to transfer data, causes a loss of amplitude on output signals.

\subsubsection{Current vs potential on material}\label{current-vs-potential}
Without opto-isolators involved, the use of the material is mostly concerned with applying voltages to one side, and reading voltages on the other side. Digital equipment typically interpret voltages without drawing much current. The diode on an opto-isolator such as opto-out, however, needs a certain amount of electric power and hence needs to draw more current. Thus, with the opto-isolator scheme without amplification, currents are to a larger degree flowing through the material. We will see in chapter \ref{ch:experiments} that current can affect the computational properties of the material.

As both input and output pins are connected to an opto-out, and no assumptions should be made to whether a pin is output or input, the resistance between the diode and ground must be kept sufficiently high so that as little voltage as possible is leaked to ground from the material through the input pins, but low enough not to lose voltage on the output diodes. In the former case, input signals could lose so much voltage that they would not be able to propagate through a high resistance material. Additionally, any pins intended to be high impedance would in fact function more like a drain. In the latter case, however, the loss of voltage on the opto-out diodes could become so large that no wave amplitude would be large enough to be transmitted through it. 

\input{fig/methodology/opto-out_resistance}

Figure \ref{fig:optores} shows the relationship between the resistance of said resistor and the two discussed potential differences. The blue plots show the difference between the material pin and ground. The red plots show the difference between the anode and cathode on the opto-out diode. We see here that while increasing the resistance gives smaller decrease in material voltage, it also decreases the voltage on the diode and hence the ability to deliver the signal to the Mecobo-side circuit. Unfortunately, with this amount of pins and resilience to statically assign pins as either input or output, it was not possible to achieve a sufficiently high diode potential so that outputs could be transmitted. 

\subsection{Amplifying the output signal}\label{ch:methodology:amp}
We have established that the amplitude of most waves coming out of the material are too low for the opto-out opto-isolator to transfer back to the Mecobo circuit. This is partly because of the resistance of the material, but mostly because of the loss of voltage described in section \ref{current-vs-potential}. This is the reason that the amplifier component of figure \ref{fig:circuit} (page \pageref{fig:circuit}) was included.

Figure \ref{fig:amp-wave} shows the effect of this amplifier through four different wave measurements. A summary of configurations for the four measurements is shown in figure \ref{fig:amp-wave-descr}. For reference, a non-amplified signal is included in figure \ref{fig:amp-wave-a}. Comparing this signal with that of figure \ref{fig:amp-wave-b}, we can see that connecting the amplifier to the circuit has no observable effect on the source signal, at least in this small scale. The differences between the non-amplified signal in figure \ref{fig:amp-wave-a} and amplified signal in figure \ref{fig:amp-wave-c} shows that the crests are amplified from $\sim750mV$ up to $\sim2.5V$, and the troughs from $\sim200mV$ down to $0V$. Figure \ref{fig:amp-wave-d} shows the amplified signal after it has been translated through opto-out. Note that this process inverts the signal. This is the actual input signal into Mecobo and, after being interpreted digitally and inverted, is what is ultimately seen by the \ac{GA}. Because the amplified amplitude is sufficiently high, this signal can represent the actual material wave. 

\input{fig/methodology/material_out_amplifier/waves}

\subsection{Amplifier's effect on computational properties}
Figure \ref{fig:methodology:many_amps} shows an example of how input pins with amplifiers connected, though unused, can affect an exhaustive sweep for one output pin. In both sweeps, pin 15 is the (amplified) output pin, and all other pins are used as input. The X axis of the graph represents the input vectors (configuration and input combined), with the far left having all input pins to zero, and the far right all ones. The Y axis represents the output value which is always zero or one. An output value of ``1'' is shown as a blue vertical line, while the white background means ``0''. Figure \ref{fig:methodology:many_amps:a} shows the negative control with only the output pin amplified, while figure \ref{fig:methodology:many_amps:b} shows the same configuration but with pins 0, 7, and 8 amplified as well. 

\input{fig/methodology/many_amps/many_amps}

The above results shows that the computational properties have slightly changed upon adding amplifiers. As we have seen in section \ref{ch:methodology:problems}, using opto-isolators without amplification resulted in a higher current flow. However, because an ideal operational amplifier has a very high input impedance, we should expect that it now causes only a very small amount of current to flow through the material. Though the computational properties are different with and without amplification, we suspect the effect is smaller when amplification is included.

%\input{fig/methodology/many_amps/osciloscope}

%Similarly, but for a single input wave, figure \ref{fig:methodology:many_amps:osciloscope} shows an oscilloscope photograph of the output difference on pin 15 with amplifiers on output only (red); and additionally input pins 0, 7, and 8 (blue). In this plot, the top waves are measured before amplification and zoomed by 2x. The bottom waves is measured after amplification. The input signal is a square wave on every other input pin.



\subsection{Frequency limitations on material input}\label{ch:methodology:frequency_limitations}
The limitations of the switching characteristics of the opto-isolotators restricts the maximum frequency of input waves to and from optowall. Figure \ref{fig:methodology:optospeed} illustrates this effect with oscilloscope photographs of nine different input frequencies of square waves from Mecobo through opto-in, measured on the corresponding material pin. The square form of the waves where found to deteriorate towards a sinus wave as the frequency increased. Additionally, starting at around 100kHz (figure \ref{fig:methodology:optospeed:100k}), the amplitude began to decrease until barely present at 1MHz (figure \ref{fig:methodology:optospeed:1M}). 

\input{fig/methodology/optospeed/optospeed}

\emph{Some} loss of amplitude on input pins may be somewhat counteracted by increasing the amplification on the output. However, a similar effect should be expected on the output opto-isolators regardless of amplification, and so an output wave of high frequency would also lose amplitude on the corresponding input\footnote{From the perspective of Mecobo} pin on Mecobo. If the crests and troughs of this signal becomes below 2V or above 0.8V respectively, the output cannot be observed by Mecobo\cite[p.~272]{mecobo14}. Additionally, there is no way to counteract the loss of shape on the waveform. In general, as the opto-isolators' ability to transfer information decreases with increased frequency, it is advised to exercise caution regarding the use of high frequencies, especially when these exceed 50kHz.

\section{Determinism of experimental tools}\label{ch:methodology:determinism}
Ultimately, the tools developed in this thesis should be utilized to explore the nature of \ac{EiM} and the computational material. Initially, there are two ways the tools can be used: Exhaustive sweeps and genetic searches.

\subsection{Exhaustive sweeps}
Exhaustive sweeps can map out the computational potential of the material in a crude but relatively deterministic way. An exhaustive sweep probes the material with every possible combination of configuration/input/output sets within a set of restrictions. The restrictions involves the type of signal, the amount of pins, and timing of the probes. We assume, for instance, that there is little point to running a probe for a lot more or less time than necessary for the output signal to stabilize. Both to keep the time consumption practical, and to be able to present the results in well-known terms, the type of wave is limited to only static voltages and the amount of pins is limited to one output pin, two inputs and the remaining for configuration. The results can then be viewed as basic logic gates such as AND, OR, XOR, etc. 

The stability of the output of individual input vectors carries some degree of indeterminism: Some input vectors does not consistently give the same output. However, as one exhaustive sweep consists of $2^{15}\cdot16$ input vectors, this indeterminism should not be a problem. In general, because of the lack of randomness in the exhaustive sweep, and the relatively stateless property of the carbon nanotubes, exhaustive sweeps are mostly deterministic. However, because of said restrictions, the results are not completely exhaustive and can only give hints of the computational properties. These hints may still be useful, though. Because of their deterministic quality, comparing exhaustive sweeps from different configurations of the experimental platform, e.g. with and without the material interface, the results might reveal interesting properties.

\subsection{Genetic searches}
Unlike exhaustive sweeps, it is impractical to attempt to use \acfp{GA} to create a map of the computational potential of a material. However, as \acp{GA} are designed to optimize a single well-defined task, they can effectively be utilized to study a few specific functions. For instance can traditional \ac{EiM} functionalities such as tone discrimination or the traveling salesman problem be evolved both with and without optowall. However, this approach is less deterministic because of the amount of randomness present in the \ac{GA}. 
