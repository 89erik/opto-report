LATEX = pdflatex
LATEXFLAGS = -shell-escape
BIBTEX = bibtex
MAKEINDEX = makeindex
PROJ_NAME = report
TEMPS = toc bbl blg log out lof lot lol aux pyg

.PHONY: clean purge upload

all: 
	-rm -f $(PROJ_NAME).pdf
	$(MAKE) $(PROJ_NAME).pdf

%.pdf:
	$(LATEX) $(LATEXFLAGS) $*.tex
	$(BIBTEX) $*.aux
	$(MAKEINDEX) $*.idx
	$(LATEX) $(LATEXFLAGS) $*.tex
	$(LATEX) $(LATEXFLAGS) $*.tex

clean:
	for s in $(TEMPS); do rm -f $(PROJ_NAME).$$s; done
	for dir in $(SUBDIRS); do $(MAKE) -C $$dir clean; done

purge: clean
	-rm -f $(PROJ_NAME).pdf

upload: 
	scp $(PROJ_NAME).pdf eriklot@caracal.stud.ntnu.no:public_html/optowall.pdf
